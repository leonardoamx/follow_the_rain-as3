package amx.utils
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import views.GameOverlay;
	/**
	 * ...
	 * @author Leonardo Molina
	 */
	public final class ScreenUtils
	{
		static public function centerChild(target:DisplayObject, parent:DisplayObject = null):void
		{
			var parent:DisplayObject = parent ? parent : target.parent;
			var point:Point = new Point (
				parent.width / 2,
				parent.height / 2
			);
			setXY (target, point);
		}

		static public function setXY(target:DisplayObject, point:Point):void
		{
trace ('x', point.x, 'y', point.y);
			target.x = point.x;
			target.y = point.y;
		}

		static public function centerOnStage(target:DisplayObject):void
		{
			var point:Point = new Point (
				target.stage.stageWidth / 2 / GameProperties.screenScale,
				target.stage.stageHeight / 2 / GameProperties.screenScale
			);
			setXY (target, point);
		}

		static public function setScale(target:DisplayObject, scale:uint):void
		{
			target.scaleX *= scale;
			target.scaleY *= scale;
		}

		static public function centerOn(target:DisplayObject, bounds:flash.geom.Rectangle):void
		{
trace (bounds.width, bounds.height);
			var point:Point = new Point (
				bounds.width / 2,
				bounds.height / 2
			);
			setXY (target, point);
		}

	}

}