package views
{
	import amx.utils.ScreenUtils;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Leonardo Molina
	 */
	public class GameOverlay extends MovieClip
	{
		public var overlay:MainTitle;
		static public const OVERLAY_DIMISSED:String = "overlayDimissed";

		public function GameOverlay()
		{
			overlay = new MainTitle ();
			overlay.mouseChildren = false;
			addChild (overlay);
			addEventListener (Event.ADDED_TO_STAGE, onAddedToStage);
		}

		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			ScreenUtils.centerOnStage (this);
		}

		public function activate ():void{
			overlay.addEventListener (TouchEvent.TOUCH_TAP, overlay_onTap);
			overlay.addEventListener (MouseEvent.CLICK, overlay_onTap);
		}

		private function overlay_onTap(e:Event):void
		{
			overlay.removeEventListener (TouchEvent.TOUCH_TAP, overlay_onTap);
			overlay.removeEventListener (MouseEvent.CLICK, overlay_onTap);
			parent.removeChild (this);
			dispatchEvent (new Event (OVERLAY_DIMISSED));
		}

	}

}