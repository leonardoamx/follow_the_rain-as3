package views
{
	import amx.utils.ScreenUtils;
	import components.Player;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Timer;

	/**
	 * ...
	 * @author Leonardo Molina
	 */
	public final class GameLevel extends MovieClip
	{
		private var timer:Timer;
		public var view:MovieClip;
		public var dropsList:Vector.<RainDrop>;
		public var touchedDropsList:Vector.<RainDrop>;
		public var player:Player;
		public var scoreField:TextField;

		public function GameLevel()
		{
			timer = new Timer (250);
			timer.addEventListener (TimerEvent.TIMER, onTimer);
			dropsList = new Vector.<RainDrop> ();
			touchedDropsList = new Vector.<RainDrop>();
			player = new Player();
			addChild (player);
			scoreField = new TextField ();
			addChild (scoreField);
			ScreenUtils.setXY (scoreField, new Point (10, 10));
			player.addEventListener (Event.ENTER_FRAME, player_onEnterFrame);
		}

		private function player_onEnterFrame(e:Event):void
		{
			dropsList.forEach (function (item:RainDrop, index:uint, array: Vector.<RainDrop>):void {
				var matched:Boolean = touchedDropsList.indexOf(item) ==-1;
				var touched:Boolean = item.hitTestObject (player);
trace ('matched ', matched, 'touched', touched);
				if (!matched && touched){
					GameProperties.incrementScore (1);
					touchedDropsList.push (item);
					scoreField.text = "Score: " + GameProperties.score;
					if (GameProperties.score > 10){
						player.updateGlow ();
					}
				}
			});
		}


		public function resume():void
		{
			timer.start();
			GameProperties.isPaused = false;
		}

		// Events

		private function onTimer(e:TimerEvent):void
		{
			var dropsAmount:uint = Math.random() * 3;
			addDrops (dropsAmount);
		}

		public function addDrops(amount:uint):void
		{
trace ("add " + amount + " drops");
			for (var a:uint = 0; a < amount; a++){
				addRandomDrop ();
			}
		}

		public function addRandomDrop():void
		{
			var point:Point = new Point (
				Math.random() * 480,
				Math.random() * 800
			);
			var scale:Number = Math.random()*.5+.5;
			addDrop (point, scale);
		}

		public function addDrop(point:Point, scale:Number):void
		{
			var dropInstance:RainDrop = new RainDrop();
			addChild (dropInstance);
			ScreenUtils.setXY (dropInstance, point);
			dropInstance.scaleX = scale;
			dropInstance.scaleY = scale;
			dropsList.push (dropInstance);
		}

		public function pause():void
		{
			timer.stop();
			GameProperties.isPaused = true;
		}
	}

}