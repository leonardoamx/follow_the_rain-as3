package
{
	import amx.utils.ScreenUtils;
	import flash.desktop.NativeApplication;
	import flash.display.MovieClip;
	import flash.display.Screen;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import views.GameLevel;
	import views.GameOverlay;

	/**
	 * ...
	 * @author Leonardo Molina
	 */
	public class Main extends Sprite
	{
		private var gameOverlay:views.GameOverlay;
		private var game:views.GameLevel;

		public function Main()
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			GameProperties.screenScale = Screen.mainScreen.bounds.width / 480;
			GameProperties.screen = Screen.mainScreen.bounds;
			ScreenUtils.setScale (this, GameProperties.screenScale);

trace ("screenScale ", GameProperties.screenScale );

			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;

			// Entry point
			// New to AIR? Please read *carefully* the readme.txt files!
			gameOverlay = new GameOverlay ();
			addChild (gameOverlay);
			gameOverlay.activate();

			gameOverlay.addEventListener (GameOverlay.OVERLAY_DIMISSED, onOverlayDismissed);
		}

		private function onOverlayDismissed(e:Event):void
		{
			if (!game){
				game = new GameLevel ();
				addChild (game);
			}
			game.resume();
		}

		private function deactivate(e:Event):void
		{
			// make sure the app behaves well (or exits) when in background
			//NativeApplication.nativeApplication.exit();
			if (game){
				game.pause();
				addChild (gameOverlay);
				gameOverlay.activate();
			}
		}

	}

}