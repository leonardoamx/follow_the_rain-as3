package components
{
	import flash.display.MovieClip;
	import flash.events.AccelerometerEvent;
	import flash.events.Event;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.sensors.Accelerometer;

	/**
	 * ...
	 * @author Leonardo Molina
	 */
	public final class Player extends MovieClip
	{
		public var glowLevel:int;
		private var accelX:Number;
		private var accelY:Number;
		private var fl_Accelerometer:Accelerometer;
		private var playerAvatar:Creature;

		public function Player()
		{
			accelX;
			accelY;

			fl_Accelerometer = new Accelerometer();
			fl_Accelerometer.addEventListener(AccelerometerEvent.UPDATE, fl_AccelerometerUpdateHandler);

			addEventListener(Event.ENTER_FRAME, onFrame);
			playerAvatar = new Creature();
			addChild (playerAvatar);
		}



		public function fl_AccelerometerUpdateHandler(event:AccelerometerEvent):void
		{
			accelX = event.accelerationX;
			accelY = event.accelerationY;
		}

		public function onFrame(evt:Event){
			if (!GameProperties.isPaused){
				x -= accelX*30;
				y += accelY*30;

				if(x > (480-width/2)){
					x = 480-width/2;
				}
				if(x < (0+width/2)){
					x = 0+width/2;
				}
				if(y > (800-width/2)){
				   y = 800-width/2;
				}
				if(y < (0+width/2)){
					y = 0+width/2;
				}
			}
		}

		public function updateGlow():void
		{
			if (GameProperties.score/10 != glowLevel){
				glowLevel = GameProperties.score / 10;
				var glowFilter:GlowFilter = new GlowFilter (0xF2B33D, .5, glowLevel, glowLevel, 2, BitmapFilterQuality.MEDIUM);
				playerAvatar.filters = [
					glowFilter
				];
			}
		}




	}

}